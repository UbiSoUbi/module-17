// Mudule17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <cmath>
using namespace std;

//1task
class JoJo 
{
private:
    string name;
    string stand;
    string meme_phrase;

 public:
     //Get data of JoJo in console
     void GetJoJo()
     {
         cout << "\nGetJoJo():\nJoJo Name:" << name << "\nStand:" << stand << "\nPhrase:" << meme_phrase;
     }
     //Return one of variable of JoJo depending on index
     string GetJoJo(char index)
     {
         switch (index)
         {
         case 'N': return name; break;
         case 'S': return stand; break;
         case 'M': return  meme_phrase;; break;
         default:  cout << "No JoJo for you!:"; break;
         }
     }
     //Set data of Jojo
     void SetJoJo(string name, string stand, string meme_phrase)
     {
         this->name = name;
         this->stand = stand;
         this->meme_phrase = meme_phrase;
     }
};
//2 task
class Vector
{
private:
    double x;
    double y;
    double z;
    //Set data of vector
public:
    void SetVector(double x, double y, double z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    //Show in console
    void Show() 
    {
        cout << "\n\n\nX:" << x << "\nY:" << y << "\nZ:" << z;
    }
    //Return the length of vector
    double Length()
    {
        return sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
    }
        

};

int main()
{

    //1 task
    string Name, Stand, Meme;
    JoJo jojo5;
    jojo5.SetJoJo("Giorno Giovanna", "Gold Experience", "I, Giorno Giovanna, have a dream." );
    jojo5.GetJoJo();


    Name = jojo5.GetJoJo('N');
    Stand = jojo5.GetJoJo('S');
    Meme = jojo5.GetJoJo('M');

    cout << "\n\n\nGetJoJo(char):\nJoJo Name:" << Name << "\nStand:" << Stand << "\nPhrase:" << Meme;
    cout << "\n\n\n";
    //jojo5.GetJoJo('m');

    //2 task
    Vector V1;
    double S;

    V1.SetVector(10, 10, 10);
    V1.Show();

    S = V1.Length();
    cout << "\nLength of vector: " << S ;
    cout << "\n\n\n";

}

